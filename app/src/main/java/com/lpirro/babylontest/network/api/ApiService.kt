package com.lpirro.babylontest.network.api

import com.lpirro.babylontest.network.models.Comment
import com.lpirro.babylontest.network.models.Post
import com.lpirro.babylontest.network.models.User
import io.reactivex.Single
import retrofit2.http.GET

interface ApiService {

    @GET("posts")
    fun getPosts(): Single<List<Post>>

    @GET("users")
    fun getUsers(): Single<List<User>>

    @GET("comments")
    fun getComments(): Single<List<Comment>>

}