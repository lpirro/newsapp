package com.lpirro.babylontest.network.api

import com.lpirro.babylontest.network.models.Comment
import com.lpirro.babylontest.network.models.Post
import com.lpirro.babylontest.network.models.User
import io.reactivex.Single

open class Repository(var apiService: ApiService = ApiServiceFactory.makeApiService()) {

    fun getPosts(): Single<List<Post>> {
        return apiService.getPosts()
    }

    fun getUsers(): Single<List<User>> {
        return apiService.getUsers()
    }

    fun getComments(): Single<List<Comment>> {
        return apiService.getComments()
    }

}
