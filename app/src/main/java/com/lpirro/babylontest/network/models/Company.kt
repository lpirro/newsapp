package com.lpirro.babylontest.network.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Company (val name: String): Parcelable