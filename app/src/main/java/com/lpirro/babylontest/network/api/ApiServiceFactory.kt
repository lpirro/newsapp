package com.lpirro.babylontest.network.api

import com.google.gson.GsonBuilder
import com.lpirro.babylontest.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object ApiServiceFactory {

    private const val BASE_URL = BuildConfig.BASE_URL

    fun makeApiService(): ApiService {
        return makeApiService(makeOkHttpClient())
    }

    private fun makeApiService(okHttpClient: OkHttpClient): ApiService {

        val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(makeGson()))
                .client(okHttpClient)
                .build()

        return retrofit.create(ApiService::class.java)
    }

    private fun makeOkHttpClient() : OkHttpClient {
        val okHttpClientBuilder = OkHttpClient().newBuilder()
        okHttpClientBuilder.retryOnConnectionFailure(false)
                .addNetworkInterceptor(makeLoggingInterceptor())

        return okHttpClientBuilder.build()

    }

    private fun makeGson() = GsonBuilder()
                .serializeNulls()
                .create()

    private fun makeLoggingInterceptor(): HttpLoggingInterceptor {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level =
                if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY
                else HttpLoggingInterceptor.Level.NONE

        return loggingInterceptor
    }
}
