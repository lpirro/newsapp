package com.lpirro.babylontest.mvp

interface View {

    fun showLoadingIndicator(active: Boolean)

    fun showError(error: Throwable)
}
