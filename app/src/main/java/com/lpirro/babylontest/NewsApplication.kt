package com.lpirro.babylontest

import android.app.Application
import com.lpirro.babylontest.app.postdetail.PostDetailModule
import com.lpirro.babylontest.app.posts.PostModule
import com.lpirro.babylontest.di.ApplicationComponent
import com.lpirro.babylontest.di.ApplicationModule
import com.lpirro.babylontest.di.DaggerApplicationComponent

class NewsApplication: Application(){

    lateinit var component: ApplicationComponent
        private set

    override fun onCreate() {
        super.onCreate()

        component = DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule(this))
            .postModule(PostModule())
            .postDetailModule(PostDetailModule())
            .build()
    }
}