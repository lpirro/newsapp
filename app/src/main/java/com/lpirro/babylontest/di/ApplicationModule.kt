package com.lpirro.babylontest.di

import android.app.Application
import android.content.Context
import com.lpirro.babylontest.network.AppSchedulerProvider
import com.lpirro.babylontest.network.SchedulerProvider
import com.lpirro.babylontest.network.api.Repository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule(val application: Application) {

    @Provides
    @Singleton
    fun provideContext(): Context = application

    @Provides
    @Singleton
    fun provideRepository(): Repository {
        return Repository()
    }

    @Provides
    fun provideschedulerProvider(): SchedulerProvider {
        return AppSchedulerProvider()
    }
}