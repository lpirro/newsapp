package com.lpirro.babylontest.di

import com.lpirro.babylontest.NewsApplication
import com.lpirro.babylontest.app.postdetail.PostDetailFragment
import com.lpirro.babylontest.app.postdetail.PostDetailModule
import com.lpirro.babylontest.app.posts.PostModule
import com.lpirro.babylontest.app.posts.PostsFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class, PostModule::class, PostDetailModule::class])
interface ApplicationComponent {
    fun inject(newsApplication: NewsApplication)
    fun inject(postsFragment: PostsFragment)
    fun inject(postDetailFragment: PostDetailFragment)
}