package com.lpirro.babylontest.app.postdetail

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.lpirro.babylontest.NewsApplication
import com.lpirro.babylontest.R
import com.lpirro.babylontest.app.extensions.show
import com.lpirro.babylontest.app.extensions.startActivityWithCheck
import com.lpirro.babylontest.network.models.Post
import com.lpirro.babylontest.network.models.User
import kotlinx.android.synthetic.main.fragment_post_detail.*
import lpirro.com.playground.app.base.BaseFragment
import javax.inject.Inject

private const val ARG_USER = "arg_user"
private const val ARG_COMMENT_NUMBER = "arg_comment_number"

class PostDetailFragment: BaseFragment(), PostsDetailContract.PostDetailView {

    override fun layoutId() = R.layout.fragment_post_detail

    @Inject lateinit var presenter: PostsDetailPresenter

    private var post: Post? = null
    private var user: User? = null
    private var commentNumber: String = ""

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        (activity?.application as NewsApplication).component.inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        presenter.attachView(this)

        post = PostDetailFragmentArgs.fromBundle(arguments).post

        if(savedInstanceState == null)
            loadData()
        else {
            val commentNumber = savedInstanceState.getString(ARG_COMMENT_NUMBER,"0")
            setCommentsNumber(commentNumber)
            user = savedInstanceState.getParcelable(ARG_USER)
            setUserUi(user)
        }

        postDetailTitle.text = post?.title
        postDetailBody.text = post?.body

        swipeRefresh.setOnRefreshListener { loadData() }
    }

    override fun setUserData(user: User) {
        this.user = user
        setUserUi(user)
    }

    override fun setCommentsNumber(commentNumber: String) {
        this.commentNumber = commentNumber
        tvCommentsNumber.text = getString(R.string.comments_in_post, commentNumber)
    }

    override fun showLoadingIndicator(active: Boolean) {
        swipeRefresh.isRefreshing = active
    }

    override fun showError(error: Throwable) {
        Toast.makeText(context, error.localizedMessage, Toast.LENGTH_SHORT).show()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putParcelable(ARG_USER, user)
        outState.putString(ARG_COMMENT_NUMBER, commentNumber)
    }

    override fun onStop() {
        super.onStop()
        presenter.unsubscribe()
    }

    private fun loadData(){
        presenter.getNumberOfComments(post!!)
        presenter.getUser(post!!.userId)
    }

    private fun setUserUi(user: User?){
        if(user == null) return

        chipCall.show()
        chipSendEmail.show()

        tvPostDetailUser.text = user.name
        tvPostDetailCompany.text = user.company.name
        tvPostDetailWebsite.text = user.website
        tvPostDetailAddress.text = user.address.city
        chipCall.text = getString(R.string.action_call, user.name)

        chipCall.setOnClickListener {
            val callIntent = Intent(Intent.ACTION_VIEW, Uri.parse("tel:${user.phone}"))
            context?.startActivityWithCheck(callIntent)
        }

        chipSendEmail.setOnClickListener {
            val emailIntent = Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:${user.email}"))
            context?.startActivityWithCheck(emailIntent)
        }
    }
}