package com.lpirro.babylontest.app.posts

import android.content.Context
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.lpirro.babylontest.NewsApplication
import com.lpirro.babylontest.R
import com.lpirro.babylontest.app.posts.adapter.PostAdapter
import com.lpirro.babylontest.network.models.Post
import kotlinx.android.synthetic.main.fragment_posts.*
import lpirro.com.playground.app.base.BaseFragment
import javax.inject.Inject

const val ARG_POSTS = "arg_posts_list"
const val ARG_LAYOUT = "arg_layout"

class PostsFragment: BaseFragment(), PostsContract.PostView, PostClickListener {

    override fun layoutId() = R.layout.fragment_posts

    private var layoutManagerId = 0
    var menu: Menu? = null

    @Inject lateinit var presenter: PostsPresenter

    private val adapter: PostAdapter by lazy {
        PostAdapter(this)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        (activity?.application as NewsApplication).component.inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)

        presenter.attachView(this)

        if(savedInstanceState == null)
            presenter.loadPosts()
        else {
            val posts = savedInstanceState.getParcelableArrayList<Post>(ARG_POSTS)
            layoutManagerId = savedInstanceState.getInt(ARG_LAYOUT)
            adapter.setData(posts)
        }

        rvPosts.layoutManager = getLayoutManager(layoutManagerId)
        rvPosts.adapter = adapter

        swipeRefresh.setOnRefreshListener { presenter.loadPosts() }
    }

    override fun onPostClick(post: Post) {
        presenter.openPostDetail(post)
    }

    override fun showPosts(posts: List<Post>) {
        adapter.setData(posts)
    }

    override fun showPostDetailUi(post: Post) {
        val postDetailDirections = PostsFragmentDirections.actionPostsFragmentToPostDetailFragment(post)
       view?.findNavController()?.navigate(postDetailDirections)
    }

    override fun showLoadingIndicator(active: Boolean) {
        swipeRefresh.isRefreshing = active
    }

    override fun showError(error: Throwable) {
        Toast.makeText(context, error.localizedMessage, Toast.LENGTH_SHORT).show()
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater) {
        this.menu = menu
        inflater.inflate(R.menu.menu_posts, menu)

        if(layoutManagerId == 0) showIconGrid()
        else showIconLinear()

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        changeLayout(item.itemId)
        return super.onOptionsItemSelected(item)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        val arrayList = ArrayList(adapter.getData())
        outState.putParcelableArrayList(ARG_POSTS, arrayList)
        outState.putInt(ARG_LAYOUT, layoutManagerId)
    }

    override fun onStop() {
        super.onStop()
        presenter.unsubscribe()
    }

    private fun changeLayout(menuId: Int){
        rvPosts.scheduleLayoutAnimation()
        when(menuId){

            R.id.action_switch_grid -> {
                showIconLinear()
                rvPosts.layoutManager = getLayoutManager(1)
            }
            R.id.action_switch_linear -> {
                showIconGrid()
                rvPosts.layoutManager = getLayoutManager(0)
            }
        }
    }

    private fun getLayoutManager(layoutManagerId: Int): RecyclerView.LayoutManager {
        return when(layoutManagerId){
            0 -> LinearLayoutManager(context)
            1 -> StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
            else -> LinearLayoutManager(context)
        }
    }

    private fun showIconGrid(){
        val itemGrid = menu?.findItem(R.id.action_switch_grid)
        val itemLinear = menu?.findItem(R.id.action_switch_linear)

        itemGrid?.isVisible = true
        itemLinear?.isVisible = false
        this.layoutManagerId = 0
    }

    private fun showIconLinear(){
        val itemGrid = menu?.findItem(R.id.action_switch_grid)
        val itemLinear = menu?.findItem(R.id.action_switch_linear)

        itemGrid?.isVisible = false
        itemLinear?.isVisible = true
        this.layoutManagerId = 1
    }
}
