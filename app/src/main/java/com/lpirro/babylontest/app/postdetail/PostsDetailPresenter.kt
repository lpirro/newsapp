package com.lpirro.babylontest.app.postdetail

import com.lpirro.babylontest.mvp.BasePresenter
import com.lpirro.babylontest.network.SchedulerProvider
import com.lpirro.babylontest.network.api.Repository
import com.lpirro.babylontest.network.models.Comment
import com.lpirro.babylontest.network.models.Post
import com.lpirro.babylontest.network.models.User
import io.reactivex.disposables.CompositeDisposable

open class PostsDetailPresenter(private val repository: Repository, private val schedulerProvider: SchedulerProvider):
    BasePresenter<PostsDetailContract.PostDetailView>(), PostsDetailContract.Presenter {

    private val mCompositeDisposable = CompositeDisposable()

    override fun unsubscribe() {
        mCompositeDisposable.clear()
    }

    override fun getNumberOfComments(post: Post) {

        view?.showLoadingIndicator(true)

        val disposable =  repository.getComments()
                .subscribeOn(schedulerProvider.ioScheduler())
                .observeOn(schedulerProvider.uiScheduler())
                .doFinally { view?.showLoadingIndicator(false) }
                .subscribe(
                        { commentsList ->
                            view?.setCommentsNumber(getCommentsForPostWithId(commentsList, post.id)) },
                        { error  ->
                            view?.showError(error) }
            )

        mCompositeDisposable.add(disposable)
    }

    override fun getUser(userId: Int) {

        view?.showLoadingIndicator(true)

        val disposable =  repository.getUsers()
                .subscribeOn(schedulerProvider.ioScheduler())
                .observeOn(schedulerProvider.uiScheduler())
                .doFinally { view?.showLoadingIndicator(false) }
                .subscribe(
                        { users -> view?.setUserData(getUserById(users, userId)) },
                        { error  -> view?.showError(error) }
                )

        mCompositeDisposable.add(disposable)
    }


    internal fun getCommentsForPostWithId(comments: List<Comment>, postId: Int): String {
        val filteredList = comments.filter { it.postId == postId }
        return filteredList.size.toString()
    }

    internal fun getUserById(users: List<User>, userId: Int): User {
        return users.first { it.id ==  userId }
    }
}