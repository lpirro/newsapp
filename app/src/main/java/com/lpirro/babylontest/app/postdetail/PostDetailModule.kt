package com.lpirro.babylontest.app.postdetail

import com.lpirro.babylontest.network.SchedulerProvider
import com.lpirro.babylontest.network.api.Repository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class PostDetailModule{

    @Provides
    @Singleton
    fun providePostDetailPresenter (repository: Repository, schedulerProvider: SchedulerProvider): PostsDetailPresenter {
        return PostsDetailPresenter(repository, schedulerProvider)
    }
}