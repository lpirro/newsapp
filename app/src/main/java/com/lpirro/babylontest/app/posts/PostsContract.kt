package com.lpirro.babylontest.app.posts

import com.lpirro.babylontest.mvp.View
import com.lpirro.babylontest.network.models.Post

interface PostsContract {

    interface PostView: View {
        fun showPostDetailUi(post: Post)
        fun showPosts(posts: List<Post>)
    }

    interface Presenter {
        fun loadPosts()
        fun openPostDetail(clickedPost: Post)
    }
}