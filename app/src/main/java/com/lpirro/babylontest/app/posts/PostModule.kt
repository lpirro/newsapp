package com.lpirro.babylontest.app.posts

import com.lpirro.babylontest.network.SchedulerProvider
import com.lpirro.babylontest.network.api.Repository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class PostModule(){

    @Provides
    @Singleton
    fun providePostPresenter (repository: Repository, schedulerProvider: SchedulerProvider): PostsPresenter {
        return PostsPresenter(repository, schedulerProvider)
    }
}