package com.lpirro.babylontest.app.extensions

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent

fun Context.startActivityWithCheck(intent: Intent){
    try {
        this.startActivity(intent)
    } catch (e: ActivityNotFoundException){
        e.printStackTrace()
    }
}