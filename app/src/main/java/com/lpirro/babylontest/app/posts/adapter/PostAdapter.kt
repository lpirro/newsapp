package com.lpirro.babylontest.app.posts.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lpirro.babylontest.R
import com.lpirro.babylontest.app.posts.PostClickListener
import com.lpirro.babylontest.network.models.Post
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.view_post.*
import java.util.*

class PostAdapter(private val listener: PostClickListener): RecyclerView.Adapter<PostAdapter.PostVH>() {

    private var data: List<Post> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostVH {
        return PostVH(
                LayoutInflater.from(parent.context)
                        .inflate(R.layout.view_post, parent, false)
        )
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: PostVH, position: Int) {
        val currentPost = data[position]

        holder.tvPostTitle.text = currentPost.title
        holder.tvPostBody.text = currentPost.body
        holder.itemView.setOnClickListener { listener.onPostClick(currentPost) }

    }

    fun setData(data: List<Post>) {
        this.data = data
        notifyDataSetChanged()
    }

    fun getData() = data

    inner class PostVH(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer
}