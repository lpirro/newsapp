package com.lpirro.babylontest.app.postdetail

import com.lpirro.babylontest.network.SchedulerProvider
import com.lpirro.babylontest.network.api.Repository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class PostsDetailModule{

    @Provides
    @Singleton
    fun providePostsDetailPresenter (repository: Repository, schedulerProvider: SchedulerProvider): PostsDetailPresenter {
        return PostsDetailPresenter(repository, schedulerProvider)
    }
}