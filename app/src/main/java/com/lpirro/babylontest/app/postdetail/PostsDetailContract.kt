package com.lpirro.babylontest.app.postdetail

import com.lpirro.babylontest.mvp.View
import com.lpirro.babylontest.network.models.Post
import com.lpirro.babylontest.network.models.User

interface PostsDetailContract {

    interface PostDetailView: View {
        fun setUserData(user: User)
        fun setCommentsNumber(commentNumber: String)
    }

    interface Presenter {
        fun getNumberOfComments(post: Post)
        fun getUser(userId: Int)
    }
}