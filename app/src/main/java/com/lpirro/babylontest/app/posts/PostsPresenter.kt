package com.lpirro.babylontest.app.posts

import com.lpirro.babylontest.mvp.BasePresenter
import com.lpirro.babylontest.network.SchedulerProvider
import com.lpirro.babylontest.network.api.Repository
import com.lpirro.babylontest.network.models.Post
import io.reactivex.disposables.CompositeDisposable

open class PostsPresenter(private val repository: Repository, private val schedulerProvider: SchedulerProvider):
    BasePresenter<PostsContract.PostView>(), PostsContract.Presenter {

    private val mCompositeDisposable = CompositeDisposable()

    override fun unsubscribe() {
        mCompositeDisposable.clear()
    }

    override fun loadPosts() {

        view?.showLoadingIndicator(true)

        val disposable =  repository.getPosts()
            .subscribeOn(schedulerProvider.ioScheduler())
            .observeOn(schedulerProvider.uiScheduler())
            .doFinally { view?.showLoadingIndicator(false) }
            .subscribe(
                { posts -> view?.showPosts(posts) },
                { error  -> view?.showError(error) }
            )

        mCompositeDisposable.add(disposable)
    }

    override fun openPostDetail(clickedPost: Post) {
        view?.showPostDetailUi(clickedPost)
    }
}