package com.lpirro.babylontest.app.posts

import com.lpirro.babylontest.network.SchedulerProvider
import com.lpirro.babylontest.network.api.Repository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class PostsModule{

    @Provides
    @Singleton
    fun providePostsPresenter (repository: Repository, schedulerProvider: SchedulerProvider): PostsPresenter {
        return PostsPresenter(repository, schedulerProvider)
    }
}