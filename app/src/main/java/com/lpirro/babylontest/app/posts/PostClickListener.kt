package com.lpirro.babylontest.app.posts

import com.lpirro.babylontest.network.models.Post

interface PostClickListener {
    fun onPostClick(post: Post)
}