package com.lpirro.babylontest.app.postdetail

import com.lpirro.babylontest.network.TestSchedulerProvider
import com.lpirro.babylontest.network.api.ApiService
import com.lpirro.babylontest.network.api.Repository
import com.lpirro.babylontest.network.models.Post
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import io.reactivex.Single
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class PostsDetailPresenterTest {
    private lateinit var presenter: PostsDetailPresenter
    private var view = mock<PostsDetailContract.PostDetailView>()
    private var service = mock<ApiService>()
    private var repo = mock<Repository>()
    private lateinit var post: Post

    @Before
    fun setup(){
        repo.apiService = service
        presenter = PostsDetailPresenter(repo, TestSchedulerProvider())
        presenter.attachView(view)
        post = Post(1, 3, "Title Test", "Body Test")
    }

    @Test
    fun `get number of comments should show comments`(){
        val comments = Single.just(MockData.generateMockCommentList())

        doReturn(comments).`when`(repo.apiService).getComments()

        presenter.getNumberOfComments(post)
        verify(view).setCommentsNumber(any())
    }

    @Test
    fun `get number of comments should show error message`(){

        doReturn(Single.error<Any>(Exception("dummy exception"))).`when`(repo.apiService).getComments()

        presenter.getNumberOfComments(post)
        verify(view).showError(any())
    }

    @Test
    fun `filter list should return zero if the list is empty`(){
        val post = Post(1, 10, "Title Test", "Body Test")
        val result = presenter.getCommentsForPostWithId(emptyList(), post.id)
        Assert.assertEquals(result, "0")
    }

    @Test
    fun `get comments for a post with id should return the list`(){
        val result = presenter.getCommentsForPostWithId(MockData.generateMockCommentList(), post.id)
        Assert.assertEquals(result, "1")
    }

    @Test
    fun `get user by id should return the user`(){
        val userId = 30
        val result = presenter.getUserById(MockData.generateMockedUserList(), userId)
        Assert.assertEquals(result.id, userId)
    }
}