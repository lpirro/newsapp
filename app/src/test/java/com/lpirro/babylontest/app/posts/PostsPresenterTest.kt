package com.lpirro.babylontest.app.posts

import com.lpirro.babylontest.app.postdetail.MockData
import com.lpirro.babylontest.network.TestSchedulerProvider
import com.lpirro.babylontest.network.api.ApiService
import com.lpirro.babylontest.network.api.Repository
import com.lpirro.babylontest.network.models.Post
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

class PostsPresenterTest {

    private lateinit var presenter: PostsPresenter
    private var view = mock<PostsContract.PostView>()
    private var service = mock<ApiService>()
    private var repo = mock<Repository>()

    @Before
    fun setup(){
        repo.apiService = service
        presenter = PostsPresenter(repo, TestSchedulerProvider())
        presenter.attachView(view)
    }

    @Test
    fun `get post should show posts`(){
        val list = MockData.generateMockPostList()
        val single: Single<List<Post>> = Single.just(list)

        doReturn(single).`when`(repo.apiService).getPosts()

        presenter.loadPosts()

        verify(view).showPosts(list)
    }

    @Test
    fun `get post should show error message`(){

        doReturn(Single.error<Any>(Exception("dummy exception"))).`when`(repo.apiService).getPosts()

        presenter.loadPosts()
        verify(view).showError(any())
    }

    @Test
    fun `get posts should show loading`(){
        val list = MockData.generateMockPostList()
        val single: Single<List<Post>> = Single.just(list)
        doReturn(single).`when`(repo.apiService).getPosts()

        val inOrder = Mockito.inOrder(view)
        presenter.loadPosts()

        inOrder.verify(view).showLoadingIndicator(true)
        inOrder.verify(view).showLoadingIndicator(false)

    }

    @Test
    fun `click on post should open post detail`(){
        val post = Post(1, 2, " Title", "Body")
        presenter.openPostDetail(post)
        verify(view).showPostDetailUi(post)
    }
}