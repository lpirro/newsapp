package com.lpirro.babylontest.app.postdetail

import com.lpirro.babylontest.network.models.*

object MockData{
    fun generateMockedUserList(): List<User> {
        return listOf(
            User(1, "Name", "Username", "email", Address(""), "", "", Company("")),
            User(3, "Name", "Username", "email", Address(""), "", "", Company("")),
            User(30, "Name", "Username", "email", Address(""), "", "", Company(""))
        )
    }

    fun generateMockCommentList(): List<Comment> {
        return listOf(
            Comment(1, 1, "Mocked Name", "Mocked Email", "Mocked comment"),
            Comment(1, 1, "Mocked Name", "Mocked Email", "Mocked comment"),
            Comment(3, 1, "Mocked Name", "Mocked Email", "Mocked comment"),
            Comment(4, 1, "Mocked Name", "Mocked Email", "Mocked comment"),
            Comment(4, 1, "Mocked Name", "Mocked Email", "Mocked comment"),
            Comment(4, 1, "Mocked Name", "Mocked Email", "Mocked comment")
        )
    }

    fun generateMockPostList(): List<Post>{
        return listOf(
            Post(1, 3, "Mock title", "Mock Body"),
            Post(1, 3, "Mock title", "Mock Body"),
            Post(1, 3, "Mock title", "Mock Body"),
            Post(1, 3, "Mock title", "Mock Body"),
            Post(1, 3, "Mock title", "Mock Body")
        )
    }
}